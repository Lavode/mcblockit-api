# Copyright (C) 2012 Michael Senn "Morrolan"
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
# documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit
# persons to whom the Software is furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
# Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# TODO: In the parsing of the player information is a list called "flags", as of now I do not know what that one might contain.
# Regex expression which would match my key, not sure if everyone's got the same structure though [0-Fa-f]{8}-[0-Fa-f]{4}-[0-Fa-f]{4}-[0-Fa-f]{4}-[0-Fa-f]{12}

# API documentation: http://desk.mcblock.it/kb/mcblockit/api-documentation

import urllib.request
import urllib.parse
import urllib.error
import json
import time
import re
import logging

class APIManager(object):

    APIPOSTURL = "http://api.mcblock.it/{0}"
    APIGETURL = "http://api.mcblock.it/userdata/{0}"
    _ValidBanTypes =   {"local": 0,
                        "global": 1,
                        "eac": 2}
    _Regex_APIKey = "[0-Fa-f]{8}-[0-Fa-f]{4}-[0-Fa-f]{4}-[0-Fa-f]{4}-[0-Fa-f]{12}"


    def __init__(self, apikey=None, api_post_url=None, api_get_url=None):
        """Initializes the APIManager, possibly overwriting the built-in API-URL.

        Parameters:
        apikey=None -- The API key which to use. Can also be set later.
        api_post_url=NoneL -- If set to anything other than None the specified URL will be used as the location of the POST-based API.
        api_get_url=NoneL -- If set to anything other than None the specified URL will be used as the location of the GET-based API.

        Exceptions:

        """
        self._APIKey = apikey
        if api_post_url is None:
            self._APIPOSTURL = APIManager.APIPOSTURL
        else:
            self._APIPOSTURL = api_post_url
        if api_get_url is None:
            self._APIGETURL = APIManager.APIGETURL
        else:
            self._APIGETURL = api_get_url

        self._cRegex_APIKey = re.compile(APIManager._Regex_APIKey)

    def _get_api_key(self):
        return self._APIKey

    def _set_api_key(self, value):
        if not self._cRegex_APIKey.match(str(value)):
            logging.warning("API key might be invalid: " + value)
        self._APIKey = str(value)

    APIKey = property(_get_api_key, _set_api_key)


    def add_local_player_ban(self, issuer, player, reason, timestamp = time.time()):
        """Locally bans the specified player.

        Parameters:
        issuer -- The admin who issued the ban.
        player -- The player whom to ban.
        reason -- The ban reason.
        timestamp=time.time() -- Current time as UNIX timestamp.

        Returns:
        bool -- True if it was successful, False if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """
        return self.add_player_ban(issuer, player, reason, timestamp, type=0)

    def add_global_player_ban(self, issuer, player, reason, timestamp=time.time()):
        """Globally bans the specified player.

        Parameters:
        issuer -- The admin who issued the ban.
        player -- The player whom to ban.
        reason -- The ban reason.
        timestamp=time.time() -- Current time as UNIX timestamp.

        Returns:
        bool -- True if it was successful, False if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """
        return self.add_player_ban(issuer, player, reason, timestamp, type=1)

    def add_player_ban(self, issuer, player, reason, timestamp=time.time(), type=0):
        """Bans the specified player.

        Parameters:
        issuer -- The admin who issued the ban.
        player -- The player whom to ban.
        reason -- The ban reason.
        timestamp=time.time() -- Current time as UNIX timestamp.

        type -- 0 or 'local' for a local ban, 1 or 'global' for a global ban.

        Returns:
        bool -- True if it was successful, False if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        ValueError -- Will be raised if you supplied an invalid ban type.
        """

        command = "ban"

        if type in APIManager._ValidBanTypes.values():
            pass
        elif type in APIManager._ValidBanTypes:
            type = APIManager._ValidBanTypes[type]
        else:
            raise ValueError("Invalid ban type supplied.")

        data = {
                "name": player,
                "admin": issuer,
                "reason": reason,
                "timestamp": timestamp,
                "type": type
               }

        try:
            result = self._do_api_post_request(command, data)
        except urllib.error.HTTPError as ex:
            if ex.code == 403:
                raise APIError(data, None, "User is already banned!")

        if result["status"] != 200:
            raise APIError(data, result)
        else:
            return True

    def remove_player_ban(self, player, timestamp=time.time()):
        """Unbans the specified player.

        Parameters:
        player -- The player whose ban to remove.
        timestamp=time.time() -- Current time as UNIX timestamp.

        Returns:
        bool -- True if it was successful, False if it failed.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """
        command = "unban"

        data = {
                "name": player,
                "timestamp": timestamp
               }

        result = self._do_api_post_request(command, data)
        if result["status"] != 200:
            raise APIError(data, result)
        else:
            return True


    def import_bans(self, banlist):
        # As of today, 13.06.2012, you seemingly can't remove the bans made via this function.
        """Imports a number of bans, up to a maximum of 50 bans per minute.

        Parameters:
        banlist -- A list of names of people whom to ban.

        Returns:
        int -- The number of people who were banned.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """
        command = "import"

        data = {"users": []}
        for banentry in banlist:
            data["users"].append(str(banentry))

        try:
            result = self._do_api_post_request(command, data)
        except urllib.error.HTTPError as ex:
            if ex.code == 429:
                raise APIError(data, None, "Too many requests.")

        if result["status"] != 200:
            raise APIError(data, result)
        else:
            # As of the 18.06.2012 the response consists of nothing but the status code.
            #return result["affected"]
            pass

    def ban_callback(self, revision_id):
        """Checks for changes in the ban list.

        Parameters:
        revision_id -- The revision ID of the banlist against which to check changes.

        Returns:
        dict -- {"Timestamp": int, "Bans": [str], "Unbans": [str]}

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """
        command = "bancheck"

        data = {"revisionID": revision_id}

        input = self._do_api_post_request(command, data)
        if input["status"] != 200:
            raise APIError(data, input)

        result = {"Timestamp": 0, "Bans": [], "Unbans": []}
        for key in input:
            if key == "status":
                pass
            elif key == "timestamp":
                result["Timestamp"] = input[key]
            elif key == "unbans":
                for banentry in input[key]:
                    result["Unbans"].append(banentry)
            elif key == "bans":
                for banentry in input[key]:
                    result["Bans"].append(banentry)

        return result


    def player_callback(self, player_dictionary):
        """Tells the MCBlock.it server which player and IPs are online currently.

        Parameters:
        player_dictionary -- {"Player1": "IP1", "Player2": "IP2"}

        Returns:
        bool -- True if it was successful.

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """
        command = "submitinfo"

        post_dictionary = {"userIPList": player_dictionary}
        result = self._do_api_post_request(command, post_dictionary)
        if result["status"] != 200:
            raise APIError(post_dictionary, result)
        else:
            return True


    def get_player_info(self, player):
        """Returns a player's bans, flags and reputation.

        Parameters:
        player -- The player whose information to query.

        Returns:
        dict -- {"Username": str, "Reputation": float, "Flags": [], "isStaff": bool, "Bans": [{"Admin": str, "Timestamp": str, "Reason": str, "Server": str}]}

        Exceptions:
        APIError -- Will be raised if the API call failed.
        """

        url = APIManager.APIGETURL.format(player)
        input = self._do_api_get_request(url)

        if input["status"] != 200:
            raise APIError(url, input)

        result = {"Username": None,
                  "Bans": [],
                  "Reputation": None,
                  "Flags": [],
                  "isStaff": None}

        for key in input:
            if key == "status":
                pass
            elif key == "username":
                result["Username"] = input[key]
            elif key == "bans":
                for banentry in input[key]:
                    ban = {"Admin": None,
                           "Timestamp": None,
                           "Reason": None,
                           "Server": None}
                    for banentry_key in banentry:
                        if banentry_key == "admin":
                            ban["Admin"] = banentry[banentry_key]
                        elif banentry_key == "timestamp":
                            ban["Timestamp"] = banentry[banentry_key]
                        elif banentry_key == "reason":
                            ban["Reason"] = banentry[banentry_key]
                        elif banentry_key == "server":
                            ban["Server"] = banentry[banentry_key]
                        else:
                            logging.info("get_player_bans/ban entry: Unexpected dictionary key found: " + banentry_key)
                    # Here the individual ban entry has been parsed.
                    result["Bans"].append(ban)
                # Here the whole banlist has been parsed.
            elif key == "reputation":
                result["Reputation"] = input[key]
            elif key == "flags":
                # As of today, 28.05.2012 I have no idea how those might be indicated, so I'll just do nothing for now.
                pass
            elif key == "isStaff":
                result["isStaff"] = input[key]
            else:
                logging.info("get_player_bans: Unexpected dictionary key found: " + key)

        return result


    def _do_api_post_request(self, command, data):
        url = self._APIPOSTURL.format(command)

        data_json = json.dumps(data)
        post_dict = {"API": self._APIKey, "data": data_json}
        post_encoded = urllib.parse.urlencode(post_dict).encode("utf-8")

        # Custom URLOpener which is used to set the user-agent to "MCBLockIt"
        opener = APIOpener()
        response = opener.open(url, post_encoded)
        result_json = response.read().decode("utf-8")
        result = json.loads(result_json)

        return result

    def _do_api_get_request(self, url):
        request = urllib.request.Request(url)
        response = urllib.request.urlopen(request)
        result_json = response.read().decode("utf-8")
        result = json.loads(result_json)

        logging.debug("-----------------")
        logging.debug("Did a GET request on: ", url)
        logging.debug("Result: ", result)
        logging.debug("-----------------")

        return result


class APIOpener(urllib.request.FancyURLopener):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.version = "MCBlockIt"

class PlayerInfo(object):
    def __init__(self, player=None, ip=None):
        """Initializes the PlayerInfo object, using the supplied IP and playername.

        Parameters:
        player=None -- The player whose information this object will contain.
        ip=None -- The IP whose information this object will contain.

        Exceptions:

        """
        self.Player = player
        #self.IP = ip
        self.Bancount = None
        self.Notecount = None
        self.Banlist = []
        self.Notelist = []

    def __str__(self, separator="------"):
        """Returns a print-ready representation of the player's information.

        Parameters:
        separator(="------") -- The separator which is used to separate individual ban entries.

        Returns:
        str -- Print-ready representation of the information regarding the player.
        Exceptions:

        """
        ban_string_list = []
        note_string_list = []

        info_list = [
            "Player: " + str(self.Player),
            "IP: " + str(self.IP),
            "Bancount: " + str(self.Bancount),
            "Notecount: " + str(self.Notecount),
            ]
        info_string = "\n".join(info_list)

        for ban in self.Banlist:
            ban_string_list.append(str(ban))
        ban_string = ("\n" + separator + "\n").join(ban_string_list)

        for note in self.Notelist:
            note_string_list.append(str(note))
        note_string = ("\n" + separator + "\n").join(note_string_list)

        return ("\n" + separator*3 + "\n").join([info_string, ban_string, note_string])


    def import_bans_from_dictionary(self, input):
        """Imports a player's ban-related information.
        Parameters:
        input -- The dictionary from which to read the bans.

        Returns:

        Exceptions:

        """
        for node in input:
            if node == "Bancount":
                self.Bancount = input["Bancount"]

            elif node == "Banlist":
                for banentry in input["Banlist"]:
                    ban = Ban()
                    ban.Player = banentry["Player"]
                    ban.import_from_dictionary(banentry)
                    if self.Player is None:
                        self.Player = banentry["Player"]
                        # Here the individual ban entry has been parsed.
                    self.Banlist.append(ban)



class Ban(object):
    def __init__(self):
        self.Player = None
        #self.IP = None
        self.Issuer = None
        self.Reason = None
        self.Server = None
        self.Timestamp = None

    def __str__(self):
        """Returns a print-ready representation of the ban.

        """
        l =  [
            "Player: " + str(self.Player),
            "IP: " + str(self.IP),
            "Issuer: " + str(self.Issuer),
            "Reason: " + str(self.Reason),
            "Server: " + str(self.Server),
            "Time: " + str(self.Timestamp)
        ]

        return "\n".join(l)

    def import_from_dictionary(self, d):
        """Imports ban details from a dictionary.

        Parameters
        d -- A dictionary containing the ban details.

        Exceptions:

        """

        try:
            self.Issuer = d["Admin"]
            self.Reason = d["Reason"]
            self.Timestamp = d["Timestamp"]
            self.Server = d["Server"]
        except KeyError as e:
            logging.warning("Missing node while importing ban details from dictionary! Ban object might be incomplete.\n" + str(d))


class APIError(Exception):
    def __init__(self, query, result, description=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.Query = query
        self.Result = result



